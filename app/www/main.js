document.addEventListener('DOMContentLoaded', () => {
    naja.initialize({history: false});

    naja.redirectHandler.addEventListener('redirect', (event) => event.detail.setHardRedirect(true))

    naja.addEventListener('success', ({detail}) => {
        if (typeof detail.payload.closeModal !== 'undefined') {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    });
});

