<?php

declare(strict_types=1);

namespace App\Controls\SortingControl;

use Nette\Application\UI\Control;
use Nette\Application\UI\Presenter;
use Nette\ComponentModel\IComponent;
use Nette\Http\IRequest;
use Nette\Http\IResponse;
use Nette\Utils\Json;


class SortingControl extends Control
{
    const  ASC = 'asc';
    const DESC = 'desc';
    const MASK_PREFIX = 'sort-';
    const COLUMN_NAME = 'column';
    const DIRECTION_NAME = 'direction';

    /** @persistent */
    public ?string $column = null;

    /** @persistent */
    public ?string $sort = null;

    private bool $ajaxRequest = false;

    public array $onShort = [];

    public string $templateFile;

    private string $cookieMask;

    private bool $saveSorting = false;

    public function __construct(
        private array              $columns,
        private string             $defaultColumn,
        private string             $defaultSort,
        private readonly IRequest  $httpRequest,
        private readonly IResponse $httpResponse,
    )
    {
        $reflection = $this->getReflection();
        $dir = dirname($reflection->getFileName());
        $name = $reflection->getShortName();
        $this->templateFile = $dir . DIRECTORY_SEPARATOR . $name . '.latte';

        $this->validateDefaultSort($this->defaultSort);

        $this->monitor(IComponent::class, function ($obj): void {
            if ($obj instanceof Presenter) {
                $this->cookieMask = self::MASK_PREFIX . $this->presenter->name . ":" . $this->name;

                if ($this->saveSorting) {
                    $value = $this->httpRequest->getCookie($this->cookieMask);
                    if (!empty($value)) {
                        $value = Json::decode($value, Json::FORCE_ARRAY);
                        if (($value[self::DIRECTION_NAME] == self::ASC || $value[self::DIRECTION_NAME] == self::DESC) &&
                            isset($this->columns[$value[self::COLUMN_NAME]])
                        ) {
                            $this->defaultColumn = $value[self::COLUMN_NAME];
                            $this->defaultSort = $value[self::DIRECTION_NAME];
                        }
                    }
                }

                if ($this->column == NULL) {
                    $this->column = $this->defaultColumn;
                }
                if ($this->sort == NULL) {
                    $this->sort = $this->defaultSort;
                }
            }
        });
    }


    private function validateDefaultSort($defaultSort): void
    {
        if ($defaultSort != self::ASC && $defaultSort != self::DESC) {
            throw new InvalidArgumentException('Parameter "' . $defaultSort . '" must be value of SortingControl::ASC or SortingControl::DESC!');
        }
    }


    public function getSortDirection(): string
    {
        if ($this->sort === self::ASC || $this->sort === self::DESC) {
            return strtoupper($this->sort);
        }

        return strtoupper($this->defaultSort);
    }


    public function getColumn(): string|array
    {
        if (isset($this->columns[$this->column])) {
            return $this->columns[$this->column];
        }

        return $this->defaultColumn;
    }

    public function getSort(): array
    {
        $sort = [];
        if (is_array($this->getColumn())) {
            foreach ($this->getColumn() as $column) {
                $sort[] = $column . ' ' . $this->getSortDirection();
            }
        } else {
            $sort[] = $this->getColumn() . ' ' . $this->getSortDirection();
        }

        return $sort;
    }


    public function setAjaxRequest(bool $value = true): static
    {
        $this->ajaxRequest = $value;
        return $this;
    }


    public function setSaveSorting(bool $value = true): static
    {
        $this->saveSorting = $value;
        return $this;
    }


    public function handleSort(string $column, string $sort): void
    {
        if (isset($this->columns[$column])) {
            $this->column = $column;
        } else {
            $this->column = $this->defaultColumn;
        }

        if ($sort === self::ASC || $sort === self::DESC) {
            $this->sort = $sort;
        } else {
            $this->sort = $this->defaultSort;
        }

        // save sorting
        if ($this->saveSorting) {
            $data = [
                self::COLUMN_NAME => $this->column,
                self::DIRECTION_NAME => $this->sort,
            ];
            $this->httpResponse->setCookie($this->cookieMask, Json::encode($data), 0);
        }

        $this->onShort($this);
    }


    public function render(string $column, ?string $title = null, ?string $direction = null)
    {
        $linkParams['column'] = $column;
        $linkParams['sort'] = self::ASC;
        $sort = '';
        $active = FALSE;

        if ($column == $this->column) {
            if ($direction) {
                $linkParams['sort'] = $sort = $direction;

                if ($linkParams['sort'] == $this->sort) {
                    $active = TRUE;
                }
            } else {
                $linkParams['sort'] = $this->sort == self::ASC ? self::DESC : self::ASC;

                if ($this->sort == self::ASC) {
                    $sort = self::ASC;
                }
                if ($this->sort == self::DESC) {
                    $sort = self::DESC;
                }
                $active = TRUE;
            }
        } elseif ($direction) {
            $linkParams['sort'] = $sort = $direction;
        }

        $url = $this->link('sort!', $linkParams);

        $template = $this->template;
        $template->url = $url;
        $template->ajaxRequest = $this->ajaxRequest;
        $template->title = $title ?: $column;
        $template->sort = $sort;
        $template->active = $active;
        $template->direction = $direction;

        $template->setFile($this->templateFile);
        $template->render();
    }
}
