<?php

declare(strict_types=1);

namespace App\Controls\SortingControl;

interface ISortingControlFactory
{
    public function create(array $columns, string $defaultColumn, string $defaultSort): SortingControl;
}
