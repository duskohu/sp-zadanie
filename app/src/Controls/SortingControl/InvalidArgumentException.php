<?php

declare(strict_types=1);

namespace App\Controls\SortingControl;

class InvalidArgumentException extends \Exception
{
}
