<?php

declare(strict_types=1);

namespace App\Controls\ItemsPerPage;


interface IItemsPerPageFactory
{
    public function create(): ItemsPerPage;
}
