<?php

declare(strict_types=1);

namespace App\Controls\ItemsPerPage;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\ComponentModel\IComponent;
use Nette\Http\IRequest;
use Nette\Http\IResponse;

class ItemsPerPage extends Control
{
    const MASK_PREFIX = 'ipp-';

    /** @persistent */
    public ?int $value = null;

    public bool $ajaxRequest = false;

    public bool $useSubmit = true;

    public string $inputLabel = 'Items per page';

    public string $submitLabel = 'Ok';

    public array $onChange = [];

    private array $perPageData = [];

    private ?int $defaultValue = null;

    private string $cookieMask;

    private string $templateFile;

    public function __construct(
        private readonly IRequest  $httpRequest,
        private readonly IResponse $httpResponse
    )
    {
        $reflection = $this->getReflection();
        $dir = dirname($reflection->getFileName());
        $name = $reflection->getShortName();
        $this->templateFile = $dir . DIRECTORY_SEPARATOR . $name . '.latte';

        $this->monitor(IComponent::class, function ($obj): void {
            if ($obj instanceof Presenter) {
                $this->cookieMask = self::MASK_PREFIX . $this->presenter->getName() . ":" . $this->getName();
            }
        });
    }

    public function setAjaxRequest(bool $value = true): self
    {
        $this->ajaxRequest = $value;
        return $this;
    }

    public function setPerPageData(array $perPageData): self
    {
        $this->perPageData = $perPageData;
        return $this;
    }

    public function setDefaultValue(int $defaultValue): self
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    public function getPerPageData(): array
    {
        $perPageData = array();
        foreach ($this->perPageData as $value) {
            $perPageData[$value] = $value;
        }
        $perPageData[$this->defaultValue] = $this->defaultValue;
        ksort($perPageData);

        return $perPageData;
    }

    public function getValue(): ?int
    {
        if (in_array($this->value, $this->getPerPageData())) {
            return $this->value;
        }

        $value = (int)$this->httpRequest->getCookie($this->cookieMask);
        if (in_array($value, $this->getPerPageData())) {
            return $value;
        } else {
            return $this->defaultValue;
        }
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $elementPrototype = $form->getElementPrototype();

        $elementPrototype->class[] = lcfirst(self::getReflection()->getShortName());
        !$this->ajaxRequest ?: $elementPrototype->class[] = 'ajax';

        $form->addSelect('itemsPerPage', $this->inputLabel, $this->getPerPageData());

        if ($this->useSubmit) {
            $form->addSubmit('change', $this->submitLabel);
        } else {
            $form['itemsPerPage']->setHtmlAttribute('data-items-per-page');
        }

        $form->onSuccess[] = $this->processSubmit(...);

        return $form;
    }


    public function processSubmit(Form $form): void
    {
        $values = $form->getValues();

        if ($values->itemsPerPage) {
            $value = $values->itemsPerPage;
        } else {
            $value = $this->defaultValue;
        }
        $this->value = $value;

        $this->httpResponse->setCookie($this->cookieMask, (string)$value, 0);
        $this->onChange($this, $this->getValue());

        if (!$this->presenter->isAjax()) {
            $this->presenter->redirect('this');
        }
    }

    public function getTemplateFile(): string
    {
        return $this->templateFile;
    }

    public function setTemplateFile(string $file): self
    {
        if ($file) {
            $this->templateFile = $file;
        }
        return $this;
    }

    public function render(): void
    {
        $template = $this->getTemplate();
        $template->_form = $template->form = $this->getComponent('form');
        $template->setFile($this->getTemplateFile());
        $template->render();
    }


    public function loadState(array $params): void
    {
        parent::loadState($params);
        $this->getComponent('form-itemsPerPage')->setDefaultValue($this->getValue());
    }
}
