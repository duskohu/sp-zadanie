<?php

declare(strict_types=1);

namespace App\Controls\BrandFormControl;

interface IBrandFormControlFactory
{
    public function create(): BrandFormControl;
}
