<?php

declare(strict_types=1);

namespace App\Controls\BrandFormControl;


use App\Forms\FormFactory;
use App\Model\Brand;
use App\Model\BrandRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class BrandFormControl extends Control
{
    public array $onAdd = [];

    public array $onEdit = [];

    private ?Brand $brandEntity = null;

    private bool $isAjax = false;

    public function __construct(
        private readonly FormFactory     $formFactory,
        private readonly BrandRepository $brandRepository,
    )
    {

    }

    public function setBrandEntity(Brand $brandEntity): void
    {
        $this->brandEntity = $brandEntity;
    }

    public function setAjax(bool $isAjax = true): void
    {
        $this->isAjax = $isAjax;
    }


    protected function createComponentForm(): Form
    {
        $form = $this->formFactory->create();
        if ($this->isAjax) {
            $elementPrototype = $form->getElementPrototype();
            $elementPrototype->class[] = 'ajax';
        }

        $form->addText('name', 'Název')
            ->setRequired('Pole název je povinné');

        $form->addSubmit('send', 'Odoslať');

        if ($this->brandEntity) {
            $form->setDefaults([
                'name' => $this->brandEntity->getName(),
            ]);

            $form->onSuccess[] = $this->edit(...);
        } else {
            $form->onSuccess[] = $this->add(...);
        }

        return $form;
    }

    private function add(Form $form, \stdClass $values): void
    {
        try {
            $this->brandRepository->addBrand($values->name);
        } catch (\Exception $e) {
            $form->addError('Něco se pokazilo zkusto někdy později!');
            return;
        }

        $this->onAdd($this);
    }

    private function edit(Form $form, \stdClass $values): void
    {
        try {
            $this->brandEntity->setName($values->name);
            $this->brandRepository->editBrand($this->brandEntity);
        } catch (\Exception $e) {
            $form->addError('Něco se pokazilo zkusto někdy později!');
            return;
        }

        $this->onEdit($this);
    }


    public function render(): void
    {
        $form = $this->getComponent('form');
        $template = $this->getTemplate();
        $template->form = $form;
        $template->render(__DIR__ . '/brandFormControl.latte');
    }
}
