<?php

declare(strict_types=1);

namespace App\Controls\VisualPaginator;

use Nette\Application\UI\Control;
use Nette\Utils\Paginator;

class VisualPaginator extends Control
{

    /** @persistent */
    public int $page = 1;

    public array $onShowPage = [];

    private string $templateFile;

    private ?Paginator $paginator = null;

    private bool $isAjax = false;

    private int $countBlocks = 4;


    public function __construct()
    {
        $reflection = $this->getReflection();
        $dir = dirname($reflection->getFileName());
        $name = $reflection->getShortName();
        $this->templateFile = $dir . DIRECTORY_SEPARATOR . $name . '.latte';
    }

    public function setCountBlocks(int $countBlocks): static
    {
        $this->countBlocks = $countBlocks;
        return $this;
    }


    public function setAjaxRequest(bool $value = TRUE): static
    {
        $this->isAjax = $value;
        return $this;
    }


    public function getPaginator(): Paginator
    {
        if (!$this->paginator) {
            $this->paginator = new Paginator;
        }
        return $this->paginator;
    }


    public function handleShowPage(int $page): void
    {
        $this->onShowPage($this, $page);
    }


    public function getTemplateFile(): string
    {
        return $this->templateFile;
    }


    public function setTemplateFile(string $file): static
    {
        if ($file) {
            $this->templateFile = $file;
        }
        return $this;
    }


    public function render(): void
    {
        $paginator = $this->getPaginator();
        $page = $paginator->page;
        if ($paginator->pageCount < 2) {
            $steps = [$page];
        } else {
            $arr = range(max($paginator->firstPage, $page - 3), min($paginator->lastPage, $page + 3));
            if ($this->countBlocks) {
                $quotient = ($paginator->pageCount - 1) / $this->countBlocks;
                for ($i = 0; $i <= $this->countBlocks; $i++) {
                    $arr[] = round($quotient * $i) + $paginator->firstPage;
                }
            }
            sort($arr);
            $steps = array_values(array_unique($arr));
        }

        $template = $this->getTemplate();
        $template->steps = $steps;
        $template->paginator = $paginator;
        $template->isAjax = $this->isAjax;
        $template->handle = 'showPage!';

        $template->setFile($this->getTemplateFile());
        $template->render();
    }

    public function loadState(array $params): void
    {
        parent::loadState($params);
        $this->getPaginator()->page = $this->page;
    }
}
