<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Controls\BrandFormControl\BrandFormControl;
use App\Controls\BrandFormControl\IBrandFormControlFactory;
use App\Controls\ItemsPerPage\IItemsPerPageFactory;
use App\Controls\ItemsPerPage\ItemsPerPage;
use App\Controls\SortingControl\ISortingControlFactory;
use App\Controls\SortingControl\SortingControl;
use App\Controls\VisualPaginator\VisualPaginator;
use App\Model\Brand;
use App\Model\BrandRepository;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\BadRequestException;

/**
 * @persistent(vp, sorting)
 */
class HomePresenter extends SecuredPresenter
{

    private ?Brand $brandEntity = null;

    public function __construct(
        private readonly EntityManagerInterface   $entityManager,
        private readonly IBrandFormControlFactory $brandFormControlFactory,
        private readonly BrandRepository          $brandRepository,
        private readonly ISortingControlFactory   $sortingControlFactory,
        private readonly IItemsPerPageFactory     $itemsPerPageFactory,
    )
    {
        parent::__construct();
    }

    public function renderDefault(): void
    {
        $countOfItems = $this->brandRepository->getCountOfItems();

        /** @var ItemsPerPage $itemsPerPage */
        $itemsPerPage = $this->getComponent('itemsPerPage');

        /** @var VisualPaginator $vp */
        $vp = $this->getComponent('vp');
        $paginator = $vp->getPaginator();
        $paginator->setItemCount($countOfItems);
        $paginator->setItemsPerPage($itemsPerPage->getValue());

        /** @var SortingControl $sorting */
        $sorting = $this->getComponent('sorting');
        $sortingDirection = $sorting->getSortDirection();
        $sortingColumns = is_array($sorting->getColumn()) ? implode(', ', $sorting->getColumn()) : $sorting->getColumn();

        $template = $this->getTemplate();
        $template->brands = $this->brandRepository->getBrands($paginator->getLength(), $paginator->getOffset(), $sortingDirection, $sortingColumns);
    }

    public function actionAdd(): void
    {
        $template = $this->getTemplate();
        $template->modalTitle = 'Pridať značku';

        if ($this->isAjax()) {
            $this->redrawControl('modal');
        } else {
            $this->redirect('default');
        }
    }

    public function actionEdit(int $id): void
    {
        $this->brandEntity = $this->entityManager->getRepository(Brand::class)->find($id);
        if ($this->brandEntity === null) {
            throw new BadRequestException();
        }

        $template = $this->getTemplate();
        $template->modalTitle = 'Upraviť značku';

        if ($this->isAjax()) {
            $this->redrawControl('modal');
        } else {
            $this->redirect('default');
        }
    }

    public function actionDelete(int $id): void
    {
        $brandEntity = $this->entityManager->getRepository(Brand::class)->find($id);
        if ($brandEntity === null) {
            throw new BadRequestException();
        }

        try {
            $this->entityManager->remove($brandEntity);
            $this->entityManager->flush();
            $this->flashMessage('Značka byla úspěšně odstraněna.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Něco se pokazilo zkusto někdy později!', 'danger');
        }

        $this->redirect('default');
    }

    protected function createComponentBrandAdd(): BrandFormControl
    {
        $control = $this->brandFormControlFactory->create();
        $control->setAjax();

        $control->onAdd[] = function (BrandFormControl $control): void {
            $this->flashMessage('Značka byla úspěšně vytvořena.', 'success');
            $this->redirect('default');
        };

        return $control;
    }

    protected function createComponentBrandEdit(): BrandFormControl
    {
        $control = $this->brandFormControlFactory->create();
        $control->setBrandEntity($this->brandEntity);
        $control->setAjax();

        $control->onEdit[] = function (BrandFormControl $control): void {
            $this->flashMessage('Značka byla úspěšně upravena.', 'success');
            $this->redirect('default');
        };

        return $control;
    }

    protected function createComponentVp(): VisualPaginator
    {
        return new VisualPaginator();
    }

    protected function createComponentSorting(): SortingControl
    {
        $columns = [
            'name' => 'b.name',
        ];

        return $this->sortingControlFactory->create($columns, 'name', SortingControl::ASC);
    }

    protected function createComponentItemsPerPage(): ItemsPerPage
    {
        $control = $this->itemsPerPageFactory->create();
        $control->setPerPageData([2, 5, 10, 20, 30, 50, 100]);
        $control->setDefaultValue(10);

        $control->onChange[] = function (ItemsPerPage $control): void {
            /** @var VisualPaginator $vp */
            $vp = $this->getComponent('vp');
            $vp->page = 1;
        };

        return $control;
    }

}