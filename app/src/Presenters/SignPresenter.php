<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Forms\SignInFormFactory;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;


final class SignPresenter extends Presenter
{
    /**
     * @Persistent
     */
    public string $backlink = '';

    public function __construct(
        private readonly SignInFormFactory $signInFactory,
    )
    {
        parent::__construct();
    }

    public function actionDefault(): void
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->flashMessage('Jste přihlášen', 'danger');
            $this->redirect('Home:');
        }
    }

    public function actionOut(): void
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->flashMessage('Nejste přihlášen', 'danger');
            $this->redirect('Sign:default');
        } else {
            $this->getUser()->logout();
            $this->flashMessage('Byl jste odhlášen', 'success');
            $this->redirect('Sign:default');
        }
    }

    protected function createComponentSignInForm(): Form
    {
        return $this->signInFactory->create(
            function (): void {
                $this->restoreRequest($this->backlink);
                $this->flashMessage('Byl jste úspěšně přihlášen', 'success');
                $this->redirect('Home:');
            });
    }
}
