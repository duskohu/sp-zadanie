<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette\Application\UI\Presenter;

abstract class SecuredPresenter extends Presenter
{

    public function startup(): void
    {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->flashMessage('Nejste přihlášen', 'danger');
            $this->redirect('Sign:');
        }
    }

}
