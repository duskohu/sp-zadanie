<?php

declare(strict_types=1);


namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;

final class SignInFormFactory
{

    public function __construct(
        private readonly FormFactory $factory,
        private readonly User        $user,
    )
    {

    }

    public function create(callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $form->addEmail('email', 'Email')
            ->setRequired('Prosím zadejte email');

        $form->addPassword('password', 'Heslo')
            ->setRequired('Prosím zadejte heslo');

        $form->addCheckbox('remember', 'Zapamatovat si mě');

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess): void {
            try {
                $this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
                $this->user->login($values->email, $values->password);
            } catch (Nette\Security\AuthenticationException $e) {
                $form->addError('Zadaný email nebo heslo je nesprávné!');
                return;
            } catch (\Exception $e) {
                $form->addError('Něco se pokazilo zkusto někdy později!');
                return;
            }
            $onSuccess();
        };

        return $form;
    }
}
