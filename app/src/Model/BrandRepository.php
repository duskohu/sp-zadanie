<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\EntityManagerInterface;

class BrandRepository
{

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function getBrands(int $limit, int $offset, string $sortingDirection, string $sortingColumns): array
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('b')
            ->from(Brand::class, 'b')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy($sortingColumns, $sortingDirection);

        return $qb->getQuery()->getResult();
    }

    public function getCountOfItems(): int
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('count(b.id)')
            ->from(Brand::class, 'b');

        return (int)$qb->getQuery()->getSingleScalarResult();

    }

    public function addBrand(string $name): void
    {
        $brand = new Brand($name);
        $this->entityManager->persist($brand);
        $this->entityManager->flush();
    }

    public function editBrand(Brand $brand): void
    {
        $this->entityManager->flush();
    }
}