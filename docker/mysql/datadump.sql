SET SESSION sql_mode = CONCAT(@@GLOBAL.sql_mode, ',STRICT_ALL_TABLES');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`
(
    `id`       int(11)      NOT NULL AUTO_INCREMENT,
    `password` varchar(255) NOT NULL,
    `email`    varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`),
    KEY `email_password` (`email`, `password`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands`
(
    `id`   int(11)      NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
